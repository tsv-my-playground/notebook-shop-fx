package ua.kiev.tsv.notebookshop.dao;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import ua.kiev.tsv.notebookshop.domain.Notebook;
import ua.kiev.tsv.notebookshop.util.HibernateUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public class NotebookDaoImpl implements NotebookDao {
    private static Logger log = Logger.getLogger(NotebookDaoImpl.class);
    private SessionFactory factory;
    private HibernateUtil hibernateUtil;

    public NotebookDaoImpl (SessionFactory factory) {
        this.factory = factory;
    }

    public NotebookDaoImpl() {
    }

    @Override
    public Long create(Notebook notebook) {
        Session session = null;
        try {
            session = factory.openSession();
            session.beginTransaction();
            Long key = (Long)session.save(notebook);
            session.getTransaction().commit();
            return key;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }

    @Override
    public Notebook read(Long ig) {
        Session session = null;
        try {
            session = factory.openSession();
            Notebook notebook = (Notebook)session.get(Notebook.class, ig);
            return notebook;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }

    @Override
    public boolean update(Notebook ntb) {
        Session session = null;
        try {
            session = factory.openSession();
            session.beginTransaction();
            session.update(ntb);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return false;
    }

    @Override
    public boolean delete(Notebook ntb) {
        Session session = null;
        try {
            session = factory.openSession();
            session.beginTransaction();
            session.delete(ntb);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return false;
    }

    @Override
    public List<Notebook> findAll() {
        List<Notebook> list;
        Session session = null;
        try {
            session = factory.openSession();
            list = session.createQuery("FROM Notebook n").list();
            return list;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            if (session != null) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }

    @Override
    public List<Notebook> findByModel(String model) {
        List<Notebook> list;
        factory = hibernateUtil.getSessionFactory();
        Session session = null;
        try {
            session = factory.openSession();
            String query = "FROM Notebook n WHERE n.model = :name";
            Query q = session.createQuery(query);
            q.setParameter("name", model);
            list = q.list();
            return list;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }

    @Override
    public List<Notebook> findByVendor(String vendor) {
        List<Notebook> list = new ArrayList<>();
        factory = hibernateUtil.getSessionFactory();
        Session session = null;
        try {
            session = factory.openSession();
            String query = "FROM Notebook n WHERE n.vendor = :name";
            Query q = session.createQuery(query);
            q.setParameter("name", vendor);
            list = q.list();
            return list;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }

    @Override
    public List<Notebook> findByPriceManufDate(Double price, Date date) {
        List<Notebook> list = new ArrayList<>();
        factory = hibernateUtil.getSessionFactory();
        Session session = null;
        try {
            session = factory.openSession();
            String query = "FROM Notebook n WHERE n.price = :name AND n.manufactureDate = :date";
            Query q = session.createQuery(query);
            q.setParameter("name", price);
            q.setParameter("date", date);
            list = q.list();
            return list;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }

    @Override
    public List<Notebook> findBetweenPriceLtDateByVendor(Double priceFrom, Double priceTo, Date date, String vendor) {
        List<Notebook> list = new ArrayList<>();
        factory = hibernateUtil.getSessionFactory();
        Session session = null;
        try {
            session = factory.openSession();
            list = session.createCriteria(Notebook.class)
                    .add(Restrictions.between("price", priceFrom, priceTo))
                    .add(Restrictions.eq("manufactureDate", date))
                    .add(Restrictions.eq("vendor", vendor)).list();
            return list;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return null;
    }
}
