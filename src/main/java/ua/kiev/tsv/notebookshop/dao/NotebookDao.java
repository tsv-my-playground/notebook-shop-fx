package ua.kiev.tsv.notebookshop.dao;

import ua.kiev.tsv.notebookshop.domain.Notebook;

import java.util.Date;
import java.util.List;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
public interface NotebookDao {
    Long create (Notebook ntb);
    Notebook read (Long ig);
    boolean update (Notebook ntb);
    boolean delete (Notebook ntb);
    List<Notebook> findAll();
    List<Notebook> findByModel(String model);
    List<Notebook> findByVendor(String vendor);
    List<Notebook> findByPriceManufDate(Double price, Date date);
    List<Notebook> findBetweenPriceLtDateByVendor(Double priceFrom, Double priceTo, Date date, String vendor);
}
