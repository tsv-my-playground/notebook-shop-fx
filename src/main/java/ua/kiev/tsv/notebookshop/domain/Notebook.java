package ua.kiev.tsv.notebookshop.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Sergiy Tsimbalyuk <sv.tsimbalyuk@gmail.com>
 */
@Entity
@Table (name = "ntb")
public class Notebook {

    @Id
    @GeneratedValue
    @Column(unique = true)
    private Long id;
    @Column (name = "SERIAL")
    private String serial;
    @Column (name = "VENDOR")
    private String vendor;
    @Column (name = "MODEL")
    private String model;
    @Column (name = "MANUFACTURE_DATE")
    private Date manufactureDate;
    @Column (name = "PRICE")
    private Double price;

    public Notebook() {
    }

    public Notebook(String serial, String vendor, String model, Date manufactureDate, Double price) {
        this.serial = serial;
        this.vendor = vendor;
        this.model = model;
        this.manufactureDate = manufactureDate;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Notebook{" +
                "id=" + id +
                ", serial=" + serial +
                ", vendor='" + vendor + '\'' +
                ", model='" + model + '\'' +
                ", manufactureDate=" + manufactureDate +
                ", price=" + price +
                '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setManufactureDate(Date manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public String getSerial() {
        return serial;
    }

    public String getVendor() {
        return vendor;
    }

    public String getModel() {
        return model;
    }

    public Date getManufactureDate() {
        return manufactureDate;
    }

    public Double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Notebook notebook = (Notebook) o;

        return (id != null ? id.equals(notebook.id) : notebook.id == null) && !(model != null ? !model.equals(notebook.model) : notebook.model != null);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (serial != null ? serial.hashCode() : 0);
        result = 31 * result + (vendor != null ? vendor.hashCode() : 0);
        result = 31 * result + (model != null ? model.hashCode() : 0);
        result = 31 * result + (manufactureDate != null ? manufactureDate.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

}
