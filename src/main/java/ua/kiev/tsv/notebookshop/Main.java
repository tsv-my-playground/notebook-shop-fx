package ua.kiev.tsv.notebookshop;

import com.github.javafaker.Company;
import com.github.javafaker.Faker;

import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import ua.kiev.tsv.notebookshop.domain.Notebook;
import ua.kiev.tsv.notebookshop.service.DeleteByModel;
import ua.kiev.tsv.notebookshop.service.DeleteWindow;
import ua.kiev.tsv.notebookshop.service.FindByPriceBetweenWindow;
import ua.kiev.tsv.notebookshop.service.FindByPriceDate;
import ua.kiev.tsv.notebookshop.service.FindByVendorWindow;
import ua.kiev.tsv.notebookshop.service.NotebookAddWindow;
import ua.kiev.tsv.notebookshop.service.NotebookServiceImpl;
import ua.kiev.tsv.notebookshop.service.PriceChangeWindow;
import ua.kiev.tsv.notebookshop.service.SerialNumberVendorWindow;

public class Main extends Application {
    private static Logger log = Logger.getLogger(Application.class);

    private NotebookServiceImpl notebookService = new NotebookServiceImpl();
    //    private Scene scene;
    private Button add = new Button("Add");
    private Button findAll = new Button("Find All");
    private Button changePrice = new Button("Change" + "\n" + "Price");
    private Button changeSerialVendor = new Button("Change" + "\n" + "Serial " + "&" + "\n" + "Vendor");
    private Button delete = new Button("Delete");
    private Button deleteByVendor = new Button("Delete" + "\n" + "by" + "\n" + "Vendor");
    private Button findByVendor = new Button("Find by" + "\n" + "Vendor");
    private Button findByPriceDate = new Button("Find by" + "\n" + "Price &" + "\n" + "Date");
    private Button findByPriceBetween = new Button("Find by" + "\n" + "Price," + "\n" + "Date &" + "\n" + "Vendor");
    private TableView<Notebook> tableView = new TableView<>();

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        fillDb();
        Locale.setDefault(Locale.ENGLISH);
        BorderPane pane = new BorderPane();
        pane.setLeft(left());
        pane.setCenter(table());
        Scene scene = new Scene (pane, 600, 600);
        primaryStage.setOnCloseRequest(event -> {
            Platform.exit();
            System.exit(0);
        });
        primaryStage.setScene(scene);
        primaryStage.setTitle("Notebook Shop");
        primaryStage.show();
        refreshTableView();
        log.info("Launched.");
    }

    private void fillDb() {
        int randomInt = ThreadLocalRandom.current().nextInt(10, 20);
        for (int i = 0; i <= randomInt; i++) {
            try {
                notebookService.add(createNewNotebook());
            } catch (Exception e) {
                log.error("Error: {}", e);
            }
        }
    }

    private Notebook createNewNotebook() {
        Faker faker = new Faker();
        Company company = faker.company();
        String serialText = UUID.randomUUID().toString().replaceAll("-", "");
        String modelText = company.buzzword();
        String vendorText = company.name();
        Date manufacture = faker.date().past(ThreadLocalRandom.current().nextInt(1, 50 + 1), TimeUnit.DAYS, new Date());
        Double priceValue = ThreadLocalRandom.current().nextDouble(100.00, 2000.00);
        return new Notebook(serialText, vendorText, modelText, manufacture, priceValue);
    }

    private TableView<Notebook> table (){
        tableView.setEditable(false);
        TableColumn idCol = new TableColumn("DB_ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn modelCol = new TableColumn("Model");
        modelCol.setCellValueFactory(new PropertyValueFactory<>("model"));
        TableColumn vendorCol = new TableColumn("Vendor");
        vendorCol.setCellValueFactory(new PropertyValueFactory<>("vendor"));
        TableColumn serialCol = new TableColumn("Serial");
        serialCol.setCellValueFactory(new PropertyValueFactory<>("serial"));
        TableColumn manufactureCol = new TableColumn("Manufacture Date");
        manufactureCol.setMinWidth(137);
        manufactureCol.setCellValueFactory(new PropertyValueFactory<>("manufactureDate"));
        TableColumn priceCol = new TableColumn("Price");
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableView.getColumns().addAll(idCol, serialCol, vendorCol, modelCol, manufactureCol, priceCol);
        return tableView;
    }

    private VBox left(){
        VBox left = new VBox();
        left.setSpacing(5);
        add.setMaxSize(75, 20);

        NotebookAddWindow naw = new NotebookAddWindow(this);
        add.setOnMouseClicked(event -> {
            try {
                naw.init();
                naw.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findAll.setMaxSize(75, 20);
        findAll.setOnMouseClicked(event -> refreshTableView());
        changePrice.setMaxWidth(75);

        PriceChangeWindow pcw = new PriceChangeWindow(this);
        changePrice.setOnMouseClicked(event -> {
            try {
                pcw.init();
                pcw.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        changeSerialVendor.setMaxWidth(75);

        SerialNumberVendorWindow snvw = new SerialNumberVendorWindow();
        changeSerialVendor.setOnMouseClicked(event -> {
            try {
                snvw.init();
                snvw.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        delete.setMaxSize(75, 20);

        DeleteWindow dw = new DeleteWindow();
        delete.setOnMouseClicked(event -> {
            try {
                dw.init();
                dw.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        deleteByVendor.setMaxWidth(75);
        DeleteByModel dbm = new DeleteByModel();
        deleteByVendor.setOnMouseClicked(event -> {
            try {
                dbm.init();
                dbm.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findByVendor.setMaxWidth(75);
        FindByVendorWindow fbvw = new FindByVendorWindow();
        findByVendor.setOnMouseClicked(event -> {
            try{
                fbvw.init();
                fbvw.start(new Stage());
            } catch (Exception e){
                e.printStackTrace();
            }
        });
        findByPriceDate.setMaxWidth(75);
        FindByPriceDate fbpd = new FindByPriceDate();
        findByPriceDate.setOnMouseClicked(event -> {
            try {
                fbpd.init();
                fbpd.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findByPriceBetween.setMaxWidth(75);
        FindByPriceBetweenWindow fbpbw = new FindByPriceBetweenWindow();
        findByPriceBetween.setOnMouseClicked(event -> {
            try {
                fbpbw.init();
                fbpbw.start(new Stage());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        left.getChildren().addAll(findAll, add, changePrice, changeSerialVendor, delete, deleteByVendor, findByVendor,
                findByPriceDate, findByPriceBetween);
        return left;
    }

    public void refreshTableView() {
        List<Notebook> list = notebookService.findAll();
        ObservableList<Notebook> observableList = FXCollections.observableArrayList();
        for (Notebook aList : list) {
            observableList.add(aList);
            observableList.sort((o1, o2) -> {
                if (o1.getId() > o2.getId()) {
                    return 1;
                } else {
                    return 0;
                }
            });
        }
        tableView.setItems(observableList);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
//        if (hibernateUtil.getSessionFactory() != null && hibernateUtil =! null){
//            hibernateUtil.getSessionFactory().close();
//        }
    }
}
