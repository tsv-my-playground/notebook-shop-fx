package ua.kiev.tsv.notebookshop.service;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import ua.kiev.tsv.notebookshop.Main;
import ua.kiev.tsv.notebookshop.domain.Notebook;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class FindByVendorWindow extends Application {


    private Main mainWindow;
    private NotebookServiceImpl notebookService = new NotebookServiceImpl();
    private Scene scene;
    private TextField vendor = new TextField();
    private Button search = new Button("Search by Vendor");
    private TableView<Notebook> tableView = new TableView<>();
    private List<Notebook> list = new ArrayList<>();

    public FindByVendorWindow(Main mainWindow) {
        this.mainWindow = mainWindow;
    }

    public FindByVendorWindow() {
    }

    @Override
    public void init() throws Exception {
        super.init();
        BorderPane pane = new BorderPane();
        HBox box = new HBox();
        box.setSpacing(10);
        vendor.setPromptText("Vendor");
        box.getChildren().addAll(vendor, search);
        pane.setTop(box);
        pane.setCenter(table());
        scene = new Scene(pane, 550, 400);
    }

    @Override
    public void stop() throws Exception {
        this.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(scene);
        primaryStage.setTitle("Find By Vendor");
        primaryStage.setMinWidth(350);
        primaryStage.setMinHeight(300);
        primaryStage.setResizable(false);
        primaryStage.show();
        search.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String vendorText = vendor.getText();
                refreshTableView(vendorText);
            }
        });
    }

    private TableView<Notebook> table (){
        tableView.setEditable(false);
        TableColumn idCol = new TableColumn("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        TableColumn serialCol = new TableColumn("Serial");
        serialCol.setCellValueFactory(new PropertyValueFactory<>("serial"));
        TableColumn vendorCol = new TableColumn("Vendor");
        vendorCol.setCellValueFactory(new PropertyValueFactory<>("vendor"));
        TableColumn modelCol = new TableColumn("Model");
        modelCol.setCellValueFactory(new PropertyValueFactory<>("model"));
        TableColumn manufactureCol = new TableColumn("Manufacture Date");
        manufactureCol.setMinWidth(137);
        manufactureCol.setCellValueFactory(new PropertyValueFactory<>("manufactureDate"));
        TableColumn priceCol = new TableColumn("Price");
        priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        tableView.getColumns().addAll(idCol, serialCol, vendorCol, modelCol, manufactureCol, priceCol);
        return tableView;
    }

    public void refreshTableView(String vendor) {
        List<Notebook> list = notebookService.findByVendor(vendor);
        ObservableList<Notebook> observableList = FXCollections.observableArrayList();
        for (int i = 0; i < list.size(); i++) {
            observableList.add(list.get(i));
            observableList.sort(new Comparator<Notebook>() {
                @Override
                public int compare(Notebook o1, Notebook o2) {
                    if (o1.getId() > o2.getId()){
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });
        }
        tableView.setItems(observableList);
    }
}


