package ua.kiev.tsv.notebookshop.service;


import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ua.kiev.tsv.notebookshop.dao.NotebookDaoImpl;
import ua.kiev.tsv.notebookshop.domain.Notebook;
import ua.kiev.tsv.notebookshop.util.HibernateUtil;

public class NotebookServiceImpl implements NotebookService {

    private static Logger log = Logger.getLogger(NotebookServiceImpl.class);
    private SessionFactory factory;

    public NotebookServiceImpl() {
    }

    @Override
    public Long add(Notebook notebook) {
        NotebookDaoImpl notebookDao = new NotebookDaoImpl(HibernateUtil.getSessionFactory());
        Long id = notebookDao.create(notebook);
        return id;
    }

    @Override
    public List<Notebook> findAll() {
        NotebookDaoImpl notebookDao = new NotebookDaoImpl(HibernateUtil.getSessionFactory());
        List<Notebook> list = notebookDao.findAll();
        return list;
    }

    @Override
    public void changePrice(Long id, double price) {
        List<Notebook> list = new ArrayList<>();
        factory = HibernateUtil.getSessionFactory();
        Session session = null;
        try {
            session = factory.openSession();
            String query = "FROM Notebook n WHERE n.id = :name";
            Query q = session.createQuery(query);
            q.setParameter("name", id);
            list = q.list();
            Notebook n = list.get(0);
            n.setPrice(price);
            session.beginTransaction();
            session.update(n);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void changeSerialVendor(Long id, String serial, String vendor) {
        List<Notebook> list = new ArrayList<>();
        factory = HibernateUtil.getSessionFactory();
        Session session = null;
        try {
            session = factory.openSession();
            String query = "FROM Notebook n WHERE n.id = :name";
            Query q = session.createQuery(query);
            q.setParameter("name", id);
            list = q.list();
            Notebook n = list.get(0);
            n.setSerial(serial);
            n.setVendor(vendor);
            session.beginTransaction();
            session.update(n);
            session.getTransaction().commit();
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public boolean delete(Long id) {
        List<Notebook> list = new ArrayList<>();
        factory = HibernateUtil.getSessionFactory();
        Session session = null;
        try {
            session = factory.openSession();
            String query = "FROM Notebook n WHERE n.id = :name";
            Query q = session.createQuery(query);
            q.setParameter("name", id);
            list = q.list();
            Notebook n = list.get(0);
            session.beginTransaction();
            session.delete(n);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return false;
    }

    @Override
    public boolean deleteByModel(String model) {
        List<Notebook> list = new ArrayList<>();
        factory = HibernateUtil.getSessionFactory();
        Session session = null;
        try {
            NotebookDaoImpl notebookDao = new NotebookDaoImpl(HibernateUtil.getSessionFactory());
            list = notebookDao.findByModel(model);
            session = factory.openSession();
            Notebook n = list.get(0);
            session.beginTransaction();
            session.delete(n);
            session.getTransaction().commit();
            return true;
        } catch (HibernateException e) {
            log.error("Open session failed", e);
            session.getTransaction().rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return false;
    }

    @Override
    public List<Notebook> findByVendor(String vendor) {
        List<Notebook> list = new ArrayList<>();
        NotebookDaoImpl ndi = new NotebookDaoImpl(HibernateUtil.getSessionFactory());
        list = ndi.findByVendor(vendor);
        return list;
    }

    @Override
    public List<Notebook> findByPriceManufDate(Double price, Date date) {
        List<Notebook> list = new ArrayList<>();
        NotebookDaoImpl ndi = new NotebookDaoImpl(HibernateUtil.getSessionFactory());
        list = ndi.findByPriceManufDate(price, date);
        return list;
    }

    @Override
    public List<Notebook> findBetweenPriceLtDateByVendor(Double priceFrom, Double priceTo, Date date, String vendor) {
        List<Notebook> list = new ArrayList<>();
        NotebookDaoImpl ndi = new NotebookDaoImpl(HibernateUtil.getSessionFactory());
        list = ndi.findBetweenPriceLtDateByVendor(priceFrom,priceTo,date,vendor);
        return list;
    }
}
