package ua.kiev.tsv.notebookshop.service;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import ua.kiev.tsv.notebookshop.Main;

public class SerialNumberVendorWindow extends Application {


    private Main mainWindow;
    private NotebookServiceImpl notebookService = new NotebookServiceImpl();
    private Scene scene;
    private TextField id = new TextField();
    private TextField serial = new TextField();
    private TextField vendor = new TextField();
    private Button changeNumberVendor = new Button("Change Number & Vendor");

    public SerialNumberVendorWindow(Main mainWindow) {
        this.mainWindow = mainWindow;
    }

    public SerialNumberVendorWindow() {
    }

    @Override
    public void init() throws Exception {
        super.init();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(2);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 25, 10, 25));
        grid.add(new Label("ID: "), 0, 1);
        grid.add(id, 1, 1);
        grid.add(new Label("Serial: "), 0, 2);
        grid.add(serial, 1, 2);
        grid.add(new Label("Vendor: "), 0, 3);
        grid.add(vendor, 1, 3);
        changeNumberVendor.setMaxWidth(Double.MAX_VALUE);
        grid.add(changeNumberVendor, 1, 4);
        scene = new Scene(grid, 350, 300);
    }

    @Override
    public void stop() throws Exception {
        this.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(scene);
        primaryStage.setTitle("Serial Number & Vendor Change");
        primaryStage.setMinWidth(350);
        primaryStage.setMinHeight(300);
        primaryStage.setResizable(false);
        primaryStage.show();
        changeNumberVendor.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Long idText = Long.valueOf(id.getText());
                String serialText = serial.getText();
                String vendorText = vendor.getText();
                notebookService.changeSerialVendor(idText, serialText, vendorText);
                primaryStage.close();
                mainWindow.refreshTableView();
            }
        });
    }
}
