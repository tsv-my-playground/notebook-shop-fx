package ua.kiev.tsv.notebookshop.service;

import org.apache.log4j.Logger;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import ua.kiev.tsv.notebookshop.Main;
import ua.kiev.tsv.notebookshop.domain.Notebook;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;


public class NotebookAddWindow extends Application {

    private static Logger log = Logger.getLogger(NotebookAddWindow.class);

    private Main mainWindow;
    private NotebookServiceImpl notebookService = new NotebookServiceImpl();
    private Scene scene;
    private TextField model = new TextField();
    private TextField vendor = new TextField();
    private TextField serial = new TextField();
    private TextField manufactureDate = new TextField();
    private TextField price = new TextField();
    private Button add = new Button("Create");

    public NotebookAddWindow(Main mainWindow) {
        this.mainWindow = mainWindow;
    }

    public NotebookAddWindow() {
    }

    @Override
    public void init() throws Exception {
        super.init();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(2);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 25, 10, 25));
        grid.add(new Label("Serial: "), 0, 1);
        grid.add(serial, 1, 1);
        grid.add(new Label("Vendor: "), 0, 2);
        grid.add(vendor, 1, 2);
        grid.add(new Label("Model: "), 0, 3);
        grid.add(model, 1, 3);
        grid.add(new Label("Manufacture date: "), 0, 4);
        manufactureDate.setPromptText("dd.MM.yyyy");
        grid.add(manufactureDate, 1, 4);
        grid.add(new Label("Price: "), 0, 5);
        grid.add(price, 1, 5);
        add.setMaxWidth(Double.MAX_VALUE);
        grid.add(add, 1, 6);
        scene = new Scene(grid, 350, 300);
    }

    @Override
    public void stop() {
        this.stop();
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setScene(scene);
        primaryStage.setTitle("Notebook adding");
        primaryStage.setMinWidth(350);
        primaryStage.setMinHeight(300);
        primaryStage.setResizable(false);
        primaryStage.show();
        add.setOnMouseClicked((MouseEvent event) -> {
            String serialText = serial.getText();
            String modelText = model.getText();
            String vendorText = vendor.getText();
            /* String dateText = manufactureDate.getText(); */
            Date manufacture = Date.valueOf(LocalDate.parse(manufactureDate.getText(),
                    DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.ENGLISH)));
            Double priceValue = Double.valueOf(price.getText());
            Notebook ntb = new Notebook(serialText, vendorText, modelText, manufacture, priceValue);
            Long ig = 0L;
            try{
                notebookService.add(ntb);
            } catch (Exception e) {
                log.error("Error: {}", e);
            }
            System.out.println(ig);
            primaryStage.close();
            mainWindow.refreshTableView();
        });
    }
}
