package ua.kiev.tsv.notebookshop.service;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import ua.kiev.tsv.notebookshop.Main;

public class PriceChangeWindow extends Application {

    private Main mainWindow;
    private NotebookServiceImpl notebookService = new NotebookServiceImpl();
    private Scene scene;
    private TextField id = new TextField();
    private TextField price = new TextField();
    private Button changePrice = new Button("Change Price");

    public PriceChangeWindow(Main mainWindow) {
        this.mainWindow = mainWindow;
    }

    public PriceChangeWindow() {
    }

    @Override
    public void init() throws Exception {
        super.init();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(2);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 25, 10, 25));
        grid.add(new Label("Serial: "), 0, 1);
        grid.add(id, 1, 1);
        grid.add(new Label("Price: "), 0, 2);
        grid.add(price, 1, 2);
        changePrice.setMaxWidth(Double.MAX_VALUE);
        grid.add(changePrice, 1, 3);
        scene = new Scene(grid, 350, 300);
    }

    @Override
    public void stop() throws Exception {
        this.stop();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(scene);
        primaryStage.setTitle("Price Change");
        primaryStage.setMinWidth(350);
        primaryStage.setMinHeight(300);
        primaryStage.setResizable(false);
        primaryStage.show();
        changePrice.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Long idText = Long.valueOf(id.getText());
                Double priceValue = Double.valueOf(price.getText());
                notebookService.changePrice(idText, priceValue);
                primaryStage.close();
                mainWindow.refreshTableView();
            }
        });
    }
}
