package ua.kiev.tsv.notebookshop.service;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import ua.kiev.tsv.notebookshop.Main;

public class DeleteByModel extends Application {


    private Main mainWindow;
    private NotebookServiceImpl notebookService = new NotebookServiceImpl();
    private Scene scene;
    private TextField model = new TextField();
    private Button delete = new Button("Delete");

    public DeleteByModel(Main mainWindow) {
        this.mainWindow = mainWindow;
    }

    public DeleteByModel() {
    }

    @Override
    public void init() throws Exception {
        super.init();
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(2);
        grid.setVgap(10);
        grid.setPadding(new Insets(10, 25, 10, 25));
        grid.add(new Label("Model"), 0, 1);
        grid.add(model, 1, 1);
        delete.setMaxWidth(Double.MAX_VALUE);
        grid.add(delete, 1, 2);
        scene = new Scene(grid, 350, 300);
    }

    @Override
    public void stop() {
        this.stop();
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setScene(scene);
        primaryStage.setTitle("Notebook Delete");
        primaryStage.setMinWidth(350);
        primaryStage.setMinHeight(300);
        primaryStage.setResizable(false);
        primaryStage.show();
        delete.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                String modelText = model.getText();
                notebookService.deleteByModel(modelText);
                primaryStage.close();
                mainWindow.refreshTableView();
            }
        });
    }
}
